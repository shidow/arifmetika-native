import React from 'react'
import { SafeAreaView, Platform, StatusBar } from 'react-native'
import Home from './components/Home'

class App extends React.Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1, marginTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0}}>
                <Home />
            </SafeAreaView>
        )
    }
}

export default App