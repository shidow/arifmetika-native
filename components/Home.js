import React from 'react'
import { BackHandler, Platform, Linking } from 'react-native'
import { WebView } from 'react-native-webview'

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.webViewRef = React.createRef()

        this.state = {
            protocol: 'https://',
            domain: 'online.arifmetika.ru'
        }
    }

    componentDidMount() {
        if (Platform.OS === 'android') BackHandler.addEventListener('hardwareBackPress', this.handleBackButton)
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
    }

    handleBackButton = () => {
        this.webViewRef.current.goBack()
        return true
    }

    handleNavigationChange(e) {
        const { domain } = this.state
        const url = e.url

        if (url.indexOf(domain) === -1 && url.indexOf('google') === -1 && url.indexOf('about:blank') === -1) {
            Linking.openURL(url)
            return false
        }
        return true
    }

    render() {
        const { protocol, domain } = this.state

        return (
            <WebView
                ref={this.webViewRef}
                source={{ uri: protocol + domain + '?os=' + Platform.OS }}
                allowsBackForwardNavigationGestures
                pullToRefreshEnabled
                startInLoadingState
                onShouldStartLoadWithRequest = {e => this.handleNavigationChange(e)}
            />
        )
    }
}

export default Home